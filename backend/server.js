const { response , request } = require('express')
const express =require ('express')
const app=express()
const cors=require('cors')
app.use(express.json())
app.use(cors('*'))
const routersbook=require('./routes/book')
app.use('/book',routersbook)
app.get('/',(request,response) => {
    response.send('welcome to backend')
})
app.listen(4000,'0.0.0.0', () =>{
    console.log("port started at 4000")
})